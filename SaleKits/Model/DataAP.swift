//
//  Data.swift
//  SaleKits
//
//  Created by devsenior1 on 10/05/2022.
//

//
//  Data.swift
//  SaleKits
//
//  Created by devsenior1 on 10/05/2022.
//

import Foundation

struct DataAP: Decodable {

    var id: String?
    var status: String?
    var createdBy: String?
    var createdDate: String?
    var lastModifiedBy: String?
    var lastModifiedDate: String?
    var numberRowEffect: Int?
    var feature1: String?
    var feature2: String?
    var feature3: String?
    var question: String?
    var answer: String?
    var nameFeature1: String?
    var nameFeature2: String?
    var nameFeature3: String?
    let createNewRecord: Bool?
    let updateRecord: Bool?
    init(json:[String:Any]) {
        self.id = json["id"] as? String
        self.status = json["status"] as? String
        self.createdBy = json["createdBy"] as? String
        self.createdDate = json["createdDate"] as? String
        self.lastModifiedBy = json["lastModifiedBy"] as? String
        self.lastModifiedDate = json["lastModifiedDate"] as? String
        self.numberRowEffect = json["numberRowEffect"] as? Int
        self.feature1 = json["feature1"] as? String
        self.feature2 = json["feature2"] as? String
        self.feature3 = json["feature3"] as? String
        self.question = json["question"] as? String
        self.answer = json["answer"] as? String
        self.nameFeature1 = json["nameFeature1"] as? String
        self.nameFeature3 = json["nameFeature3"] as? String
        self.createNewRecord = json["createNewRecord"] as? Bool
        self.updateRecord = json["updateRecord"] as? Bool
    }
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case status = "status"
        case createdBy = "createdBy"
        case createdDate = "createdDate"
        case lastModifiedBy = "lastModifiedBy"
        case lastModifiedDate = "lastModifiedDate"
        case numberRowEffect = "numberRowEffect"
        case feature1 = "feature1"
        case feature2 = "feature2"
        case feature3 = "feature3"
        case question = "question"
        case answer = "answer"
        case nameFeature1 = "nameFeature1"
        case nameFeature2 = "nameFeature2"
        case nameFeature3 = "nameFeature3"
        case createNewRecord = "createNewRecord"
        case updateRecord = "updateRecord"
    }

}
