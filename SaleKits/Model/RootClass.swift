//
//  RootClass.swift
//  SaleKits
//
//  Created by devsenior1 on 10/05/2022.
//

import Foundation

//  API.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.



import Foundation
class API : Decodable{
    let code: String
    let message: String
    let warningCode: String
    let dataA: [DataAP]

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case warningCode = "warningCode"
        case dataA = "data"
    }

    required init(from decoder: Decoder) throws {

        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.code = try values.decodeIfPresent(String.self, forKey: .code) ?? ""
        self.message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.warningCode = try values.decodeIfPresent(String.self, forKey: .warningCode) ?? ""
        self.dataA = try values.decode([DataAP].self, forKey: .dataA)
    }
}
