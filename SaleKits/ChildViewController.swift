//
//  ChildViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class ChildViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var childTableview: UITableView!
    public var dataApi: [DataAP] = [DataAP]()
    var childNumber: String = ""
    var token:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        childTableview.register(UINib(nibName: "QASanTableViewCell", bundle: nil), forCellReuseIdentifier: "QASanTableViewCell")
        childTableview.dataSource = self
        childTableview.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callApiData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.dataApi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = childTableview.dequeueReusableCell(withIdentifier:"QASanTableViewCell", for: indexPath) as! QASanTableViewCell
        cell.sanphamLB.text = "\(self.dataApi[indexPath.row].question ?? "")"
        cell.cauhoiLB.text = "\(self.dataApi[indexPath.row].nameFeature1 ?? "")"
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
    func callApiData() {
        let headers:HTTPHeaders = [
                    "Authorization": "Bearer \(token)",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/qa/get-by-question?question=", method: .get, encoding: JSONEncoding.default, headers: headers)
            .response { [weak self] (responseData) in
                let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)
                
                print("abc123===", str)
                                guard let strongSelf = self, let data = responseData.data else {
                                    return
                                }
                                do {
                                    let character = try JSONDecoder().decode(API.self, from: data)
                                    print("\(character)>>>>>>>>>")
                                        strongSelf.dataApi = character.dataA
                                        strongSelf.childTableview.reloadData()
                                } catch {
                                    print("Error \(error)")
            }
    }
}
}
