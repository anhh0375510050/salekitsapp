//
//  ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
   
    let parameters: [String: Any] = [ "username" : "aa", "password" : "bb" ]
    @IBOutlet weak var khdnBtn: UIButton!
    @IBOutlet weak var khcnBtn: UIButton!
    @IBOutlet weak var viewKHCN: UIView!
    @IBOutlet weak var viewDN: UIView!
    @IBOutlet weak var viewconDN: UIView!
    @IBOutlet weak var viewMK: UIView!
    @IBOutlet weak var viewconMK: UIView!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passWordTxt: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewKHCN.layer.cornerRadius = 10
        viewDN.layer.cornerRadius = 10
        viewconDN.layer.cornerRadius = 10
        viewMK.layer.cornerRadius = 10
        viewconMK.layer.cornerRadius = 10
        loginBtn.layer.cornerRadius = 10
    }
    
    func callApiLogin() {
        let parameters: [String: Any] = [
            "username" : "itsol_test01",
            "password" : "123456a@",
        ]

        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/ldap-authenticate", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                            case .success(let value):
                                if let json = value as? [String: Any] {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
                                    vc.token = json["id_token"] as! String
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    print("value ==",json["id_token"])
                                } else {
                                    
                                }
                            case .failure(let error):
                                print(error)
                            }
            }
    }

   
    @IBAction func khdnButton(_ sender: Any) {
        khcnBtn.setImage(UIImage(named: "KHCN.png"), for: .normal)
        khdnBtn.setImage(UIImage(named: "KHDN.png"), for: .normal)
    }
    
    @IBAction func khcnButton(_ sender: Any) {
        khcnBtn.setImage(UIImage(named: "KHCND.png"), for: .normal)
        khdnBtn.setImage(UIImage(named: "KHDND.png"), for: .normal)
    }
    @IBAction func loginButton(_ sender: Any) {
//        if usernameTxt.text == "itsol_test01", passWordTxt.text == "123456a@" {
                      callApiLogin()
//                  }else{
//                      let alert = UIAlertController(title: "Thông báo", message: "Thông tin đăng nhập không chính xác.", preferredStyle: UIAlertController.Style.alert)
//                              alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertAction.Style.default, handler: nil))
//                              self.present(alert, animated: true, completion: nil)
//                  }
    }
}

  


